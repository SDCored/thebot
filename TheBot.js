const fs = require("fs");
const {
  creatorName,
  creatorLogo,
  prefix,
  discordToken,
  botCommandsChannelID,
  modCasesChannelID,
  miscLogsChannelID,
  memberLogsChannelID,
  serverLogsChannelID,
  voiceLogsChannelID,
  messageLogsChannelID,
  joinLeaveLogsChannelID,
} = require("./config.json");
const { version, lastUpdated } = require("./package.json");
const Discord = require("discord.js"); // Loading discord.js for communicating with the Discord API
const client = new Discord.Client(); // Creating a new discord client that acts as the bot
client.commands = new Discord.Collection();
var moment = require("moment"); // MomentJS

const commandFiles = fs
  .readdirSync("./commands")
  .filter((file) => file.endsWith(".js"));

for (const file of commandFiles) {
  const command = require(`./commands/${file}`);

  // set a new item in the collection
  // with the ket as the command name
  // and the value as the exported module
  client.commands.set(command.name, command);
}

// Runs when the client connects to Discord
client.on("ready", () => {
  console.log("Logging in...");
  console.log("Logged in as", client.user.tag);

  // Sets the bots status to online
  client.user.setStatus("online");

  // Set the bots presence to inform the user how to use the bot
  client.user.setPresence({
    game: {
      name: "lots of hentai",
      type: 3, // Watching
    },
  });

  // When there is a message on the server...
  client.on("message", async (message) => {
    // If the author of the message is the bot, ignore it
    if (message.author.equals(client.user)) return;

    // If the message doesn't begin with +, ignore it
    if (!message.content.startsWith("+")) return;

    // Log used commands in the console
    console.log(
      `(${moment(message.createdTimestamp).format("MM/DD/YYYY hh:mm:ss A")})[${
        message.author
      }] ${message}`
    );

    // Converting the command and args to lowercase for ease of use
    // (Really hope this doesn't become a problem later)
    const args = message.content
      .toLowerCase()
      .slice(prefix.length)
      .trim()
      .split(/ +/g);
    const commandName = args.shift().toLowerCase();

    if (!client.commands.has(commandName)) return;

    const creatorName = `TheBot ${version} [${lastUpdated}]`;
    const creatorLogo = "https://placehold.it/100x100";

    const command = client.commands.get(commandName);

    try {
      command.execute(client, message, args);
    } catch (error) {
      console.log(error);
      message.reply("Not a valid command.");
    }

    // Help Command
    if (command === "help") {
      const helpEmbed = new Discord.RichEmbed()
        .setTitle(`TheBot Help`)
        .setAuthor("TheBot", client.user.avatarURL)
        .setColor("ed5555")
        .setDescription(
          `If you want more information on commands, type ${prefix}commands. Otherwise, here is a short overview.

          **Current Bot Prefix: **${prefix}`
        )
        .setFooter(creatorName, creatorLogo)
        .setTimestamp()
        .addField(`${prefix}help`, "Shows this information.", true)
        .addField(
          `${prefix}server`,
          "Shows information about the current server.",
          true
        );

      return message.channel.send(helpEmbed);
    }

    // Bot Command
    if (command === "bot") {
      const helpEmbed = new Discord.RichEmbed()
        .setTitle(`TheBot`)
        .setAuthor("TheBot", client.user.avatarURL)
        .setColor("ed5555")
        .setDescription(`TheBot; Hentai Discord Moderation`)
        .setFooter(creatorName, creatorLogo)
        .setTimestamp()
        .addField("Bot Version", `${version}`, true)
        .addField("Last Updated", `${lastUpdated}`, true);

      return message.channel.send(helpEmbed);
    }

    // Info Command
  });
});

// When a message is deleted
client.on("messageDelete", function (message) {
  if (message.channel.id == botCommandsChannelID) return;
  if (message.channel.id == modCasesChannelID) return;
  if (message.channel.id == miscLogsChannelID) return;
  if (message.channel.id == memberLogsChannelID) return;
  if (message.channel.id == serverLogsChannelID) return;
  if (message.channel.id == voiceLogsChannelID) return;
  if (message.channel.id == messageLogsChannelID) return;
  if (message.channel.id == joinLeaveLogsChannelID) return;

  var messageAttachments = message.attachments;

  let attachmentList = "";
  messageAttachments.forEach(
    (attachment) => (attachmentList += attachment.url + ", ")
  );

  function messageExists(messageText) {
    if (messageText.content) {
      return message;
    } else {
      return "None";
    }
  }

  function hasAttachments() {
    if (message.attachments.size > 0) {
      return attachmentList.substring(0, attachmentList.length - 2);
    } else {
      return "None";
    }
  }

  const embed = new Discord.RichEmbed()
    .setTitle(`Message Deleted in #${message.channel.name}`)
    .setAuthor(message.author.tag, message.author.avatarURL)
    .setColor("dc3545")
    .setFooter(creatorName, creatorLogo)
    .setTimestamp()
    .addField("Message Contents", `${messageExists(message)}`, true)
    .addField("Message Attachments", `${hasAttachments()}`, true);

  client.channels.get(messageLogsChannelID).send(embed);
});

// When a message is updated
client.on("messageUpdate", function (oldMessage, newMessage) {
  if (oldMessage.channel.id === botCommandsChannelID) return;
  if (oldMessage.channel.id === modCasesChannelID) return;
  if (oldMessage.channel.id === miscLogsChannelID) return;
  if (oldMessage.channel.id === memberLogsChannelID) return;
  if (oldMessage.channel.id === serverLogsChannelID) return;
  if (oldMessage.channel.id === voiceLogsChannelID) return;
  if (oldMessage.channel.id === messageLogsChannelID) return;
  if (oldMessage.channel.id === joinLeaveLogsChannelID) return;

  function messageOld() {
    if (oldMessage.content) {
      return oldMessage;
    } else {
      return "None";
    }
  }

  function messageNew() {
    if (newMessage.content) {
      return newMessage;
    } else {
      return "None";
    }
  }

  const embed = new Discord.RichEmbed()
    .setTitle(`Message Edited in #${oldMessage.channel.name}`)
    .setAuthor(oldMessage.author.tag, oldMessage.author.avatarURL)
    .setColor("dc3545")
    .setFooter(creatorName, creatorLogo)
    .setTimestamp()
    .addField("Before", `${messageOld()}`, true)
    .addField("After", `${messageNew()}`, true);

  client.channels.get(messageLogsChannelID).send(embed);
});

// Login to Discord
client.login(discordToken);
