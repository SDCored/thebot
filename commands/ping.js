const Discord = require("discord.js");
const { creatorName, creatorLogo } = require("../config.json");

module.exports = {
  name: "ping",
  description: "Pong!",
  execute(client, message) {
    const pingEmbed = new Discord.RichEmbed()
      .setTitle(`Dropship Ping`)
      .setAuthor("Dropship", message.client.user.avatarURL)
      .setColor("ed5555")
      .setFooter(creatorName, creatorLogo)
      .setTimestamp()
      .setDescription("Pong!")
      .addField(
        "Latency",
        `${new Date().getTime() - message.createdTimestamp}ms`,
        true
      );

    message.channel.send(pingEmbed);
  },
};
