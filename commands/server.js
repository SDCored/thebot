const Discord = require("discord.js");
const { creatorName, creatorLogo } = require("../config.json");
var moment = require("moment"); // MomentJS

module.exports = {
  name: "server",
  description: "Server Information",
  execute(client, message) {
    const serverEmbed = new Discord.RichEmbed()
      .setTitle(`Server Info for ${message.guild.name}`)
      .setAuthor("Dropship", client.user.avatarURL)
      .setColor("ed5555")
      .setDescription(
        `TheBot; Hentai Discord Moderation\n\n**Server Owner: **${
          message.guild.owner
        } (ID: ${message.guild.ownerID})\n**Server Created On:** ${moment(
          message.guild.createdAt
        ).format("MMMM Do YYYY hh:mm:ss A")}\n**Server Features: ** ${
          message.guild.features
        }\n**Server Region: **${message.guild.region}`
      )
      .setFooter(creatorName, creatorLogo)
      .setThumbnail(`${message.guild.iconURL}`)
      .setTimestamp()
      .addField(
        "Members",
        `Total: **${message.guild.memberCount}/250,000**`,
        true
      )
      .addField(
        "Channels",
        `Total: **${message.guild.channels.size}/500**`,
        true
      )
      .addField("Roles", `Total: **${message.guild.roles.size}/250**`, true);

    message.channel.send(serverEmbed);
  },
};
